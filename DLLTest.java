class Node<T>
{
        T val;
        Node prev,next;
        Node(T val)
        {
                this.val=val;
        }
}
class DoubleLinkList<T>
{
        private Node<T> head,tail;
        public int addFront(Node<T> n) // add to right
        {
                if(head == null && tail == null)
                        head = tail = n;
                else
                {
                        tail.next = n;
                        n.prev = tail;
                        tail = n;
                }
                return 0;
        }
        public int addBack(Node<T> n) // add to left
        {
                if(tail == null && tail == null)
                        head = tail = n;
                else
                {
                        head.prev = n;
                        n.next = head;
                        head = n;
                }
                return 0;
        }
        public int removeFront()  //remove from right
        {
             if(tail == null){
               	System.out.println("rmF(): List empty");
             	return 0;
            }      
            /**throw new Exception(){};
            else**/ if(head == tail)
                   head = tail = null;
            else
            {
                   tail = tail.prev;
                   tail.next = null;
            }
            return 0;
        }
        public int removeBack()  // remove from left
        {
        	if(tail == null){
            	System.out.println("rmB(): List empty");
            	return 0;
            }
                        /**throw new Exception(){};
             else**/ if(head == tail)
            	 head = tail = null;
             else
             {
                 head = head.next;
                 head.prev = null;
             }
             return 0;
        }
        /**public String print() // print full list
        {
            String str = new String();
            Node<T> curr = head;
            if(curr == null){
            	str = "print():List empty";
            }
            else{
            	str += "[";
            	do
            	{
            		str += "\t" + curr.val.toString();
            		curr = curr.next;
            	}
            	while(curr != null);
            	str += "]";
            }
            return str;
        }**/
        @Override
        public String toString(){
        	String str = new String();
            Node<T> curr = head;
            if(curr == null){
            	str = "print():List empty";
            }
            else{
            	str += "[";
            	do
            	{
            		str += "\t" + curr.val.toString();
            		curr = curr.next;
            	}
            	while(curr != null);
            	str += "]";
            }
            return str;
        	
        }
}
public class DLLTest
{
        public static void main(String a[])
        {
                /**DoubleLinkList<Integer> list = new DoubleLinkList();
                list.addFront(new Node<Integer>(5));
                list.addFront(new Node<Integer>(4));
                list.addBack(new Node<Integer>(1));
                list.addBack(new Node<Integer>(8));
                System.out.println(list.print());
                list.removeBack();
                System.out.println(list.print());
                list.removeFront();
                System.out.println(list.print());
                list.removeBack();
                System.out.println(list.print());
                list.removeFront();
                System.out.println(list.print());
                list.removeFront();
                System.out.println(list.print());***/
                DoubleLinkList<String> slist = new DoubleLinkList();
                slist.addFront(new Node<String>("1st"));
                slist.addBack(new Node<String>("2st"));
                slist.addBack(new Node<String>("3st"));
                slist.addFront(new Node<String>("4st"));
                System.out.println(slist);
                slist.removeBack();
                System.out.println(slist);
                slist.removeFront();
                System.out.println(slist);
                slist.removeBack();
                System.out.println(slist);
                slist.removeFront();
                System.out.println(slist);
                slist.removeFront();
                System.out.println(slist);
        }
}